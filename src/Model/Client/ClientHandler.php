<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 18.06.18
 * Time: 16:23
 */

namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = $this->encodePlainPassword($data['password']);
        $client->setPassword($password);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);

        return $client;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return md5($password) . md5($password . '2');
    }
}
