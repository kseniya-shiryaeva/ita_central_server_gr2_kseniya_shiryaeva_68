<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOneByPassportOrEmail(string $passport, string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport = :passport')
                ->orWhere('a.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmail(string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByPasswordAndEmail($password, $email)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter('password', $password)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }



    /**
     * @param string $social_id
     * @return Client|null
     */
    public function findOneByFacebook(string $social_id)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.faceBookId = :social_id')
                ->setParameter(
                    'social_id', $social_id
                )
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $social_id
     * @return Client|null
     */
    public function findOneByVkontacte(string $social_id)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.vkId = :social_id')
                ->setParameter(
                    'social_id', $social_id
                )
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $social_id
     * @return Client|null
     */
    public function findOneByGoogle(string $social_id)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.googleId = :social_id')
                ->setParameter(
                    'social_id', $social_id
                )
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}
