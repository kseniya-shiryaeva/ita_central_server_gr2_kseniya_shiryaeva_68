<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client/password/encode", name="app_client_password_encode")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function passwordEncodeAction(
        ClientHandler $clientHandler,
        Request $request
    )
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePlainPassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check_client_credentials/{encodedPassword}/{email}", name="app_check_client_credentials")
     * @Method("HEAD")
     * @param string $encodedPassword
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientCredentialsAction(
        string $encodedPassword,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($encodedPassword, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportOrEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/client/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $user = $clientRepository->findOneByEmail($email);
        if ($user) {
            return new JsonResponse($user->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/{network}/network/{identity}", name="app_client_by_network")
     * @Method("GET")
     * @param string $network
     * @param string $identity
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByNetworkAction(
        string $network,
        string $identity,
        ClientRepository $clientRepository)
    {
        switch ($network){
            case 'facebook':
                $user = $clientRepository->findOneByFacebook($identity);
                break;
            case 'google':
                $user = $clientRepository->findOneByGoogle($identity);
                break;
            case 'vkontakte':
                $user = $clientRepository->findOneByVkontacte($identity);
                break;
        }

        if ($user) {
            return new JsonResponse($user->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }



    /**
     * @Route("/set_network_to_client", name="app_set_network_to_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function setNetworkToClientAction(
        ClientRepository $clientRepository,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');

        $client = $clientRepository->findOneByEmail($data['email']);

        if($client){
            $client->setVkId($data['vkId']);
            $client->setGoogleId($data['googleId']);
            $client->setFaceBookId($data['faceBookId']);

            $manager->persist($client);
            $manager->flush();

            return new JsonResponse(['result' => 'ok']);
        } else{
            throw new NotFoundHttpException();
        }

    }
}
